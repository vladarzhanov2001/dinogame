#include "common.h"
#include "st7789.h"
#include "joystick.h"

extern const uint8_t dino_normal[];
extern const uint8_t font_arial_normal[];


int main(void)
{
    SysCtlClockSet(SYSCTL_SYSDIV_2_5 | SYSCTL_USE_PLL | SYSCTL_OSC_MAIN | SYSCTL_XTAL_16MHZ);//80 MHz

    FPULazyStackingEnable();
    FPUEnable();

    scr_init();

    uint16_t green = scr_color(0,255,0);
    uint16_t black = scr_color(0,0,0);
    uint16_t white = scr_color(255,255,255);

    scr_fill(black);


    drawBitmap(0, 80, dino_normal, 64, 64, white, black);

    jsk_init();

     int i=0;
     int jumping = 0;
     int dinoMove = 0;


     while(1) {
         uint32_t AdcValues[1];
         ADCIntClear(JSK_ADCY_BASE,3);
         ADCProcessorTrigger(JSK_ADCY_BASE,3);
         while(!ADCIntStatus(JSK_ADCY_BASE,3,false));
         ADCSequenceDataGet(JSK_ADCY_BASE,3,AdcValues);
         if(AdcValues[0]<2100) {
             jumping = 0;
         } else if(AdcValues[0]>2200 & AdcValues[0]<4095) {
             if (jumping == 0) jumping = 1;
         }

         if (jumping == 0) {
             dinoMove = (dinoMove + 1) % 3;
         } else {
             if (jumping == 1 && dinoMove < 80) {
                  dinoMove = dinoMove + 8;
                  if (dinoMove >= 80) jumping = 2;
                } else {
                  dinoMove = dinoMove - 8;
                  if (dinoMove < 8) {
                    jumping = 0;
                    dinoMove = 0;
                  }
             }
         }


         switch (dinoMove) {
           case -1: drawBitmap(0, 80, dino_normal, 64, 64, white, black); break;
           case 0: drawBitmap(0, 80, dino_normal, 64, 64, white, black); break;
           case 1: drawBitmap(0, 80, dino_normal, 64, 64, white, black); break;
           case 2: drawBitmap(0, 80, dino_normal, 64, 64, white, black); break;
           default:
               drawBitmap( 0, 80 - dinoMove, dino_normal, 64, 64, white, black);
               drawBitmap( 0, 80 - dinoMove, dino_normal, 64, 64, black, black);
               break;
         }


//        SysCtlDelay(SysCtlClockGet()/8);
//
//         if(i<0)
//             i=0;
//
//         i=i % 10;
//
//         scr_fill(0);
//
//
//         if(i==0) scr_str(40, 40, black, green, "one()");
//         else if(i==1) scr_str(40, 40, black, green, "one()");
//         else if(i==2) scr_str(40, 40, black, green, "two()");
//         else if(i==3) scr_str(40, 40, black, green, "three()");
//         else if(i==4) scr_str(40, 40, black, green, "fore()");
//         else if(i==5) scr_str(40, 40, black, green, "five()");
//         else if(i==6) scr_str(40, 40, black, green, "six");
//         else if(i==7) scr_str(40, 40, black, green, "seven()");
//         else if(i==8) scr_str(40, 40, black, green, "eigth()");
//         else if(i==9) scr_str(40, 40, black, green, "nine()");
     }

}

