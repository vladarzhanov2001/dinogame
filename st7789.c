/*
 * st7789.c
 *
 *  Created on: Dec 23, 2020
 *      Author: dlysenko
 */


#include "st7789.h"

#include "common.h"


#define ST7789_TFTWIDTH     240
#define ST7789_TFTHEIGHT    240

#define ST7789_240x240_XSTART 0
#define ST7789_240x240_YSTART 0

#define ST_CMD_DELAY   0x80    // special signifier for command lists

#define ST7789_NOP     0x00
#define ST7789_SWRESET 0x01
#define ST7789_RDDID   0x04
#define ST7789_RDDST   0x09

#define ST7789_SLPIN   0x10
#define ST7789_SLPOUT  0x11
#define ST7789_PTLON   0x12
#define ST7789_NORON   0x13

#define ST7789_INVOFF  0x20
#define ST7789_INVON   0x21
#define ST7789_DISPOFF 0x28
#define ST7789_DISPON  0x29
#define ST7789_CASET   0x2A
#define ST7789_RESET   0x2B
#define ST7789_RAMWR   0x2C
#define ST7789_RAMRD   0x2E

#define ST7789_PTLAR   0x30
#define ST7789_COLMOD  0x3A
#define ST7789_MADCTL  0x36

#define ST7789_MADCTL_MY  0x80
#define ST7789_MADCTL_MX  0x40
#define ST7789_MADCTL_MV  0x20
#define ST7789_MADCTL_ML  0x10
#define ST7789_MADCTL_RGB 0x00

#define ST7789_RDID1   0xDA
#define ST7789_RDID2   0xDB
#define ST7789_RDID3   0xDC
#define ST7789_RDID4   0xDD

#define ST7789_TEOFF  0x34
#define ST7789_TEON   0x35


// Color definitions
#define BLACK   0x0000
#define BLUE    0x001F
#define RED     0xF800
#define GREEN   0x07E0
#define CYAN    0x07FF
#define MAGENTA 0xF81F
#define YELLOW  0xFFE0
#define WHITE   0xFFFF

#define DC_HIGH  GPIOPinWrite(SCR_PIN_BASE, SCR_PIN_DC, SCR_PIN_DC)
#define DC_LOW   GPIOPinWrite(SCR_PIN_BASE, SCR_PIN_DC, 0)
#define RST_HIGH GPIOPinWrite(SCR_PIN_BASE, SCR_PIN_RST, SCR_PIN_RST)
#define RST_LOW  GPIOPinWrite(SCR_PIN_BASE, SCR_PIN_RST, 0)

#define ST7789_COLMOD_16BITCOLOR 0x55


uint16_t  _xstart=0;
uint16_t  _ystart=0;
uint16_t  _colstart =ST7789_240x240_XSTART;
uint16_t  _rowstart=ST7789_240x240_YSTART;

extern const uint8_t font_arial_normal[];
#define TMPSTRLEN  100
char _tmpstr[TMPSTRLEN+1]={0};


uint8_t scr_spi(uint8_t c)
{
    while(SSIBusy( SCR_SSI_BASE )) {}
    SSIDataPut( SCR_SSI_BASE,c);

/*
    uint32_t r;
    SSIDataGet(SCR_SSI_BASE, &r);
    r &= 0x00FF;

    return (uint8_t)r;
    */
    return 0;

}

void scr_cmd(uint8_t c)
{
   DC_LOW;
   scr_spi(c);
}


void scr_dat(uint8_t c)
{
   DC_HIGH;
   scr_spi(c);
}


void scr_dat2(uint16_t c)
{
   DC_HIGH;

   while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c >> 8) == 0)  {}
   while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c) == 0)  {}

   while(SSIBusy(SCR_SSI_BASE)) {}
}

void scr_dat2N(uint16_t c,uint16_t n)
{
   DC_HIGH;

   int32_t i;

   for(i=0;i<n;i++)
   {
       while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c >> 8) == 0)  {}
       while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c) == 0)  {}
   }
   while(SSIBusy(SCR_SSI_BASE)) {}
}


void scr_datext(GetNextPixelCallback proc,void* tag,uint16_t n)
{
   DC_HIGH;

   int32_t i;
   uint16_t c;

   for(i=0;i<n;i++)
   {
       c = proc(tag);
       while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c >> 8) == 0)  {}
       while ( SSIDataPutNonBlocking(SCR_SSI_BASE,c) == 0)  {}
   }
   while(SSIBusy(SCR_SSI_BASE)) {}
}

void scr_delay(uint32_t ms) {
    const uint32_t onems = 20000;
    SysCtlDelay(onems*ms);
}

uint16_t scr_color(uint8_t r,uint8_t g,uint8_t b) {
    r = r >> 3;
    g = g >> 2;
    b = b >> 3;
    uint16_t c = (r << 11) | (g << 5) | (b);
    return c;
}

void scr_rotation(uint8_t m) {
  scr_cmd(ST7789_MADCTL);
  int rotation = m % 4;

  switch (rotation) {
       case 0:
         scr_dat(ST7789_MADCTL_MX | ST7789_MADCTL_MY | ST7789_MADCTL_RGB);
         _xstart = _colstart;
         _ystart = _rowstart;
         break;
       case 1:
         scr_dat(ST7789_MADCTL_MY | ST7789_MADCTL_MV | ST7789_MADCTL_RGB);
         _ystart = _colstart;
         _xstart = _rowstart;
         break;
      case 2:
         scr_dat(ST7789_MADCTL_RGB);
         _xstart = _colstart;
         _ystart = _rowstart;
         break;

       case 3:
         scr_dat(ST7789_MADCTL_MX | ST7789_MADCTL_MV | ST7789_MADCTL_RGB);
         _ystart = _colstart;
         _xstart = _rowstart;
         break;
  }
}

void scr_init() {
    SYSENABLE(SCR_SSI_PIN_PERIPH)
    SYSENABLE(SCR_SSI_PERIPH);
    SYSENABLE(SCR_PIN_PERIPH);

    SysCtlDelay(3);

    GPIOPinConfigure(SCR_SSI_CLK);
    GPIOPinConfigure(SCR_SSI_FSS);
    GPIOPinConfigure(SCR_SSI_RX);
    GPIOPinConfigure(SCR_SSI_TX);

    GPIOPinTypeSSI(SCR_SSI_PINS_BASE, SCR_SSI_PINS );

    SSIConfigSetExpClk(SCR_SSI_BASE, SysCtlClockGet(), SSI_FRF_MOTO_MODE_2, SSI_MODE_MASTER, SCR_SPIFREQ, 8);

    SSIEnable(SCR_SSI_BASE);

    GPIOPinTypeGPIOOutput(SCR_PIN_BASE, SCR_PIN_DC | SCR_PIN_RST );

    RST_LOW;
    scr_delay(50);
    RST_HIGH;
    scr_delay(50);

    scr_cmd(ST7789_SWRESET);
    scr_delay(150);

    scr_cmd(ST7789_SLPOUT);
    scr_delay(500);

    scr_cmd(ST7789_COLMOD);
    scr_dat(ST7789_COLMOD_16BITCOLOR);
    scr_delay(10);

    scr_cmd(ST7789_MADCTL);
    scr_dat(0x00);

    scr_cmd(ST7789_CASET);
    scr_dat(0x00);
    scr_dat(ST7789_240x240_XSTART);
    scr_dat((ST7789_TFTWIDTH+ST7789_240x240_XSTART) >> 8);
    scr_dat((ST7789_TFTWIDTH+ST7789_240x240_XSTART) & 0xFF);

    scr_cmd(ST7789_RESET);
    scr_dat(0x00);
    scr_dat(ST7789_240x240_YSTART);
    scr_dat((ST7789_TFTHEIGHT+ST7789_240x240_YSTART) >> 8);
    scr_dat((ST7789_TFTHEIGHT+ST7789_240x240_YSTART) & 0xFF);

    scr_cmd(ST7789_INVON);
    scr_delay(10);

    scr_cmd(ST7789_NORON);
    scr_delay(10);

    scr_cmd(ST7789_DISPON);
    scr_delay(500);


    scr_rotation(2);
}

void scr_addr(uint16_t x0, uint16_t y0, uint16_t x1, uint16_t y1) {

  uint16_t x_start = x0 + _xstart, x_end = x1 + _xstart;
  uint16_t y_start = y0 + _ystart, y_end = y1 + _ystart;

  scr_cmd(ST7789_CASET);
  scr_dat(x_start >> 8);
  scr_dat(x_start & 0xFF);
  scr_dat(x_end >> 8);
  scr_dat(x_end & 0xFF);

  scr_cmd(ST7789_RESET);
  scr_dat(y_start >> 8);
  scr_dat(y_start & 0xFF);
  scr_dat(y_end >> 8);
  scr_dat(y_end & 0xFF);

  scr_cmd(ST7789_RAMWR);
}

void scr_dot(int16_t x, int16_t y, uint16_t color) {
  if((x < 0) ||(x >= ST7789_TFTWIDTH ) || (y < 0) || (y >= ST7789_TFTHEIGHT )) return;

  scr_addr(x,y,x+1,y+1);

  scr_dat2(color);
}


void scr_fillext(int16_t x, int16_t y, int16_t w, int16_t h, GetNextPixelCallback proc,void* tag)
{
    //todo: check sizes

    scr_addr(x, y, x+w-1, y+h-1);
    scr_datext(proc,tag,h*w);

}

void scr_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color)
{
    if((x >= ST7789_TFTWIDTH) || (y >= ST7789_TFTHEIGHT )) return;
    if((x + w - 1) >= ST7789_TFTWIDTH)  w = ST7789_TFTWIDTH  - x;
    if((y + h - 1) >= ST7789_TFTHEIGHT ) h = ST7789_TFTHEIGHT  - y;

    scr_addr(x, y, x+w-1, y+h-1);


    uint32_t n = h*w;

    scr_dat2N(color,n);
}

void scr_fill(uint16_t color)
{
    //scr_cmd(ST7789_TEON); // not working
    scr_rect(0, 0,  ST7789_TFTWIDTH, ST7789_TFTHEIGHT, color);
    //scr_cmd(ST7789_TEOFF);
}

void scr_charext(int x,int y,uint16_t fg,uint16_t bg,char c)
{
    int i,j,h,w;

    volatile uint16_t a,b,pa,pb;
    w=16;
    h=16;


    if((x >= ST7789_TFTWIDTH) || (y >= ST7789_TFTHEIGHT )) return;
    if((x + w - 1) >= ST7789_TFTWIDTH)  w = ST7789_TFTWIDTH  - x;
    if((y + h - 1) >= ST7789_TFTHEIGHT ) h = ST7789_TFTHEIGHT  - y;

    int x0 = x<0 ? 0 : x;
    int y0 = y<0 ? 0 : y;
    int x1 = x+w-1;
    int y1 = y+h-1;

    if ((x1<0) && (y1<0))
        return;


    scr_addr(x0, y0, x1, y1);

    uint16_t one=1;
    int pos = 4+32*(c-' '+1);
    int xx,yy;

    for(i=0;i<h;i++)
    {
            pa =pos-(16-i)*2;
            pb = pos-(16-i)*2+1;
            a = font_arial_normal[pa];
            b = font_arial_normal[pb];
            uint16_t f = (a << 8) | b;

            for(j=0;j<w;j++)
            {

                uint16_t mask = one << (15-j);
                uint16_t color = ((f  & mask)!=0) ? fg : bg;

                xx = x+j;
                yy = y+i;

                if ((xx>=0) && (yy>=0))
                    scr_dat2(color);
            }

    }

}

void scr_char(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,char c)
{
    uint32_t i,j;

    volatile uint16_t a,b,pa,pb;
    uint32_t w=16;
    uint32_t h=16;

    if((x >= ST7789_TFTWIDTH) || (y >= ST7789_TFTHEIGHT )) return;
    if((x + w - 1) >= ST7789_TFTWIDTH)  w = ST7789_TFTWIDTH  - x;
    if((y + h - 1) >= ST7789_TFTHEIGHT ) h = ST7789_TFTHEIGHT  - y;


    scr_addr(x, y, x+w-1, y+h-1);

    uint16_t one=1;
    int pos = 4+32*(c-' '+1);

    for(i=0;i<h;i++)
    {


            pa =pos-(16-i)*2;
            pb = pos-(16-i)*2+1;
            a = font_arial_normal[pa];
            b = font_arial_normal[pb];
            uint16_t f = (a << 8) | b;

            for(j=0;j<w;j++)
            {

                uint16_t mask = one << (15-j);
                uint16_t color = ((f  & mask)!=0) ? fg : bg;

                scr_dat2(color);

            }

    }

}

void scr_scroll(int y,int dx,uint16_t fg,uint16_t bg,char* str)
{

    int dxpixel =  dx % 16;
    int n = strlen(str);
    int m = ST7789_TFTWIDTH / 16;

    int pos = dx / 16;
    pos = -pos;
    pos = pos % n;
    int i=0;
    int x=0;
    while(i<=m)
    {   scr_charext(x+dxpixel,y,fg,bg,str[pos]);
        x+=16;
        pos++;
        if (pos>=n)
            pos = 0;
        i++;
    }

}


void scr_str(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,char* str)
{
    while(*str!=0)
    {   scr_char(x,y,fg,bg,*str);
        x+=16;
        str++;
    }
}


void itoa(int n, char s[]);

void scr_int(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,int n)
{
    itoa(n,_tmpstr);
    scr_str(x,y,fg,bg,_tmpstr);
}

void reverse(char s[])
{
    int c, i, j;

    for (i = 0, j = strlen(s)-1; i<j; i++, j--) {
        c = s[i];
        s[i] = s[j];
        s[j] = c;
    }
}

/* itoa:  convert n to characters in s */
void itoa(int n, char s[])
{
    int i, sign;

    if ((sign = n) < 0)  /* record sign */
        n = -n;          /* make n positive */
    i = 0;

    //s[i++] = ' ';
    //s[i++] = ' ';
    //s[i++] = ' ';
    //s[i++] = ' ';

    do {       /* generate digits in reverse order */
        s[i++] = n % 10 + '0';   /* get next digit */
    } while ((n /= 10) > 0);     /* delete it */
    if (sign < 0)
        s[i++] = '-';


    s[i++] = '\0';


    reverse(s);
}

void drawBitmap(int x, int y, const uint16_t* bitmap, int width, int height, uint16_t fgColor, uint16_t bgColor) {
    int i, j;

    int x0 = x < 0 ? 0 : x;
    int y0 = y < 0 ? 0 : y;
    int x1 = x + width - 1;
    int y1 = y + height - 1;

    if ((x1 < 0) || (y1 < 0) || (x0 >= ST7789_TFTWIDTH) || (y0 >= ST7789_TFTHEIGHT))
        return;

    if ((x0 + width - 1) >= ST7789_TFTWIDTH) width = ST7789_TFTWIDTH - x0;
    if ((y0 + height - 1) >= ST7789_TFTHEIGHT) height = ST7789_TFTHEIGHT - y0;

    scr_addr(x0, y0, x1, y1);

    int pixelIndex = 0;
    uint16_t pixelColor;

    for (i = 0; i < height; i++) {
        for (j = 0; j < width; j++) {
            pixelColor = (bitmap[pixelIndex++] != 0) ? fgColor : bgColor;
            scr_dat2(pixelColor);
        }
    }
    while(SSIBusy(SCR_SSI_BASE)) {}
}

