/*
 * st7789.h
 *
 *  Created on: Dec 23, 2020
 *      Author: dlysenko
 */

#ifndef ST7789_H_
#define ST7789_H_

#include <stdint.h>

typedef uint16_t (*GetNextPixelCallback)(void* ptr);
void scr_fillext(int16_t x, int16_t y, int16_t w, int16_t h, GetNextPixelCallback proc,void* tag);

void scr_test();
void scr_init();
void scr_fill(uint16_t color);
void scr_char(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,char c);
void scr_str(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,char* str);
void scr_int(uint16_t x,uint16_t y,uint16_t fg,uint16_t bg,int n);
uint16_t scr_color(uint8_t r,uint8_t g,uint8_t b);
void scr_rect(int16_t x, int16_t y, int16_t w, int16_t h, uint16_t color);

void drawBitmap(int x, int y, const uint16_t* bitmap, int width, int height, uint16_t fgColor, uint16_t bgColor);

void scr_charext(int x,int y,uint16_t fg,uint16_t bg,char c);
void scr_scroll(int y,int dx,uint16_t fg,uint16_t bg,char* str);

#endif /* ST7789_H_ */
