/*
 * common.h
 *
 *  Created on: Dec 27, 2020
 *      Author: dlysenko
 */

#ifndef COMMON_H_
#define COMMON_H_




#include <stdint.h>
#include <stdbool.h>
#include <string.h>

#include "inc/hw_ints.h"
#include "inc/hw_memmap.h"
#include "inc/hw_nvic.h"
#include "inc/hw_types.h"
#include "driverlib/debug.h"
#include "driverlib/fpu.h"
#include "driverlib/gpio.h"
#include "driverlib/interrupt.h"
#include "driverlib/pin_map.h"
#include "driverlib/rom.h"
#include "driverlib/sysctl.h"
#include "driverlib/systick.h"
#include "driverlib/udma.h"

#include "inc/hw_gpio.h"
#include "inc/hw_ssi.h"
#include "inc/hw_types.h"
#include "driverlib/ssi.h"
#include "utils/uartstdio.h"
#include "driverlib/uart.h"

#include "driverlib/adc.h"

#include <string.h>




#define SYSENABLE(A)      SysCtlPeripheralEnable(A);while(!SysCtlPeripheralReady(A)){};

#define UNLOCK_PF0          HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;HWREG(GPIO_PORTF_BASE + GPIO_O_CR) |= 0x01;HWREG(GPIO_PORTF_BASE + GPIO_O_AFSEL) |= 0x400;HWREG(GPIO_PORTF_BASE + GPIO_O_DEN) |= 0x01;HWREG(GPIO_PORTF_BASE + GPIO_O_LOCK) = 0;
#define UNLOCK_PD7          HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = GPIO_LOCK_KEY;HWREG(GPIO_PORTD_BASE + GPIO_O_CR) |= 0x80;HWREG(GPIO_PORTD_BASE + GPIO_O_AFSEL) &= ~0x80;HWREG(GPIO_PORTD_BASE + GPIO_O_DEN) |= 0x80;HWREG(GPIO_PORTD_BASE + GPIO_O_LOCK) = 0;



#define printf     UARTprintf




//      ST7789
//PB4 - CLK
//PB5 - CS
//PB6 -
//PB7 - DIN
//PA6 - RST
//PA7 - DC


#define SCR_PIN_BASE        GPIO_PORTA_BASE
#define SCR_PIN_DC          GPIO_PIN_7
#define SCR_PIN_RST         GPIO_PIN_6
#define SCR_PIN_PERIPH      SYSCTL_PERIPH_GPIOA


#define SCR_SSI_PIN_PERIPH  SYSCTL_PERIPH_GPIOB
#define SCR_SSI_PERIPH      SYSCTL_PERIPH_SSI2
#define SCR_SSI_CLK         GPIO_PB4_SSI2CLK
#define SCR_SSI_FSS         GPIO_PB5_SSI2FSS
#define SCR_SSI_RX          GPIO_PB6_SSI2RX
#define SCR_SSI_TX          GPIO_PB7_SSI2TX
#define SCR_SSI_BASE        SSI2_BASE
#define SCR_SSI_PINS        (GPIO_PIN_4 | GPIO_PIN_5 | GPIO_PIN_6 |  GPIO_PIN_7)
#define SCR_SSI_PINS_BASE   GPIO_PORTB_BASE

#define SCR_SPIFREQ         40000000


//      LED
//PF1 - RED
//PF2 - GREEN
//PF3 - BLUE

#define LED_PIN_RED         GPIO_PIN_1
#define LED_PIN_GREEN       GPIO_PIN_3
#define LED_PIN_BLUE        GPIO_PIN_2
#define LED_PIN_ALL         (LED_PIN_RED|LED_PIN_GREEN|LED_PIN_BLUE)
#define LED_PIN_PERIPH      SYSCTL_PERIPH_GPIOF
#define LED_PIN_BASE        GPIO_PORTF_BASE

//      Joystick
//PE3 - adc, y
//PE2 - adc, x
//PF0 - button B,SW2, pull-up
//PF4 - button A,SW1, pull-up

#define JSK_ADC_PERIPH      SYSCTL_PERIPH_GPIOE

#define JSK_ADCY_PERIPH     SYSCTL_PERIPH_ADC0
#define JSK_ADCY_BASE       ADC0_BASE
#define JSK_ADCY_CH         ADC_CTL_CH0
#define JSK_ADCY_PIN        GPIO_PIN_3

#define JSK_ADCX_PERIPH     SYSCTL_PERIPH_ADC1
#define JSK_ADCX_BASE       ADC1_BASE
#define JSK_ADCX_CH         ADC_CTL_CH1
#define JSK_ADCX_PIN        GPIO_PIN_2

#define JSK_SW2_UNLOCK      UNLOCK_PF0

#define JSK_SW_PERIPH       SYSCTL_PERIPH_GPIOF
#define JSK_SW_BASE         GPIO_PORTF_BASE
#define JSK_SW1_PIN         GPIO_PIN_4
#define JSK_SW2_PIN         GPIO_PIN_0
#define JSK_SW_PINS         (JSK_SW1_PIN|JSK_SW2_PIN)

//                MCP4921
//
// 3.3v -> VDD -  [*   ] - VOUT  ->
//         CS  -  [    ] - VSS   <- GND
//         SCK -  [    ] - VREF  <- 3.3v
//         SDI -  [    ] - LDAC  <- GND to push data on every CS=HIGH

//      MCP4921
//PA5 - SDI
//PA3 - CS
//PA2 - SCK

#define SND_PIN_PERIPH         SYSCTL_PERIPH_GPIOA
#define SND_SSI_PERIPH         SYSCTL_PERIPH_SSI0

#define SND_PIN_CLK            GPIO_PA2_SSI0CLK
#define SND_PIN_FSS            GPIO_PA3_SSI0FSS
#define SND_PIN_TX             GPIO_PA5_SSI0TX
#define SND_PIN_BASE           GPIO_PORTA_BASE
#define SND_PIN_ALL            (GPIO_PIN_2 |GPIO_PIN_3|GPIO_PIN_5)

#define SND_SSI_BASE           SSI0_BASE

#define SND_UDMA_CH            UDMA_CHANNEL_SSI0TX

#define SND_INT                INT_SSI0

#endif /* COMMON_H_ */
