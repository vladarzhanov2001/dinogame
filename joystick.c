#include "joystick.h"
#include "common.h"
#include "driverlib/adc.h"


void jsk_init() {
    // SW
    SysCtlPeripheralEnable(JSK_SW_PERIPH);
    GPIOPinTypeGPIOInput(JSK_SW_BASE, JSK_SW_PINS);
    GPIOPadConfigSet(JSK_SW_BASE, JSK_SW2_PIN, GPIO_STRENGTH_2MA, GPIO_PIN_TYPE_STD_WPU);

    // Y
    SysCtlPeripheralEnable(JSK_ADCY_PERIPH);
    GPIOPinTypeADC(JSK_SW_BASE, JSK_ADCY_PIN);
    ADCHardwareOversampleConfigure(JSK_ADCY_BASE, 64);
    ADCSequenceDisable(JSK_ADCY_BASE, 3);
    ADCSequenceConfigure(JSK_ADCY_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
    ADCSequenceStepConfigure(JSK_ADCY_BASE , 3 , 0 , JSK_ADCY_CH | ADC_CTL_IE | ADC_CTL_END);
    ADCSequenceEnable(JSK_ADCY_BASE, 3);
//
//    // X
//    SysCtlPeripheralEnable(JSK_ADCX_PERIPH);
//    GPIOPinTypeADC(GPIO_PORTE_BASE, JSK_ADCX_PIN);
//    ADCHardwareOversampleConfigure(JSK_ADCX_BASE, 64);
//    ADCSequenceDisable(JSK_ADCX_BASE, 3);
//    ADCSequenceConfigure(JSK_ADCX_BASE, 3, ADC_TRIGGER_PROCESSOR, 0);
//    ADCSequenceStepConfigure(JSK_ADCX_BASE , 3 , 0 , JSK_ADCX_CH | ADC_CTL_IE | ADC_CTL_END);
//    ADCSequenceEnable(JSK_ADCX_BASE, 3);
}

